package com.example.daggertest.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.daggertest.R
import com.example.daggertest.Smth
import com.example.daggertest.app.IApp
import com.example.daggertest.di.component.ActivityComponent
import com.example.daggertest.di.provider.ActivityProvider
import com.example.daggertest.fragment.FragmentA
import com.example.daggertest.fragment.FragmentB
import javax.inject.Inject

class MainActivity : AppCompatActivity(), IActivity {

    private lateinit var provider: ActivityComponent

    @Inject
    lateinit var smth: Smth

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        println("!!! activity smth=${smth.hashCode()}")

        supportFragmentManager.beginTransaction()
            .add(R.id.master, FragmentA.newInstance())
            .add(R.id.details, FragmentB.newInstance())
            .commit()
    }

    private fun inject() {
        provider = ActivityComponent.Initializer
            .init((applicationContext as IApp).getComponent())
        provider.inject(this)
    }

    override fun getComponent(): ActivityProvider {
        return provider
    }
}