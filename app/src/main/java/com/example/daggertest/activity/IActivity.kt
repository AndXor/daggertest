package com.example.daggertest.activity

import com.example.daggertest.di.provider.ActivityProvider

interface IActivity {

    fun getComponent() : ActivityProvider

}