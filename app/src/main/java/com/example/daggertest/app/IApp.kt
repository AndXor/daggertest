package com.example.daggertest.app

import com.example.daggertest.di.provider.ApplicationProvider

interface IApp {

    fun getComponent(): ApplicationProvider
}