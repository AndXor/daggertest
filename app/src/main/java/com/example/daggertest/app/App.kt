package com.example.daggertest.app

import android.app.Application
import com.example.daggertest.di.component.ApplicationComponent
import com.example.daggertest.di.provider.ApplicationProvider

class App : Application(), IApp {

    private val applicationComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        ApplicationComponent.Initializer
            .init(this@App)
    }

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    private fun inject() {
        applicationComponent.inject(this)
    }

    override fun getComponent(): ApplicationProvider {
        return applicationComponent
    }
}