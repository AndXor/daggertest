package com.example.daggertest.di.component

import com.example.daggertest.di.provider.ActivityProvider
import com.example.daggertest.fragment.FragmentA
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(dependencies = [ActivityProvider::class])
interface FragmentAComponent {

    fun inject(fragmentA: FragmentA)

    class Initializer private constructor() {

        companion object {

            fun init(activity: ActivityProvider): FragmentAComponent = DaggerFragmentAComponent.builder()
                .activityProvider(activity)
                .build()
        }
    }
}