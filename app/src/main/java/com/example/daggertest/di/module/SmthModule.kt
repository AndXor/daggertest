package com.example.daggertest.di.module

import com.example.daggertest.Smth
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SmthModule {

    @Provides
    @Singleton
    fun provideSmth() = Smth()
}