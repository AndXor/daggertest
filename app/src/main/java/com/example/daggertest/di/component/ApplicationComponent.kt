package com.example.daggertest.di.component

import com.example.daggertest.app.App
import com.example.daggertest.app.IApp
import com.example.daggertest.di.provider.ApplicationProvider
import dagger.Component
import javax.inject.Singleton

@Component
@Singleton
interface ApplicationComponent : ApplicationProvider {

    fun inject(app: App)

    class Initializer private constructor() {

        companion object {
            fun init(app: IApp): ApplicationComponent {
                return DaggerApplicationComponent.builder()
                    .build()
            }
        }
    }
}