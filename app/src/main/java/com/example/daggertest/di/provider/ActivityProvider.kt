package com.example.daggertest.di.provider

import com.example.daggertest.Smth

interface ActivityProvider {

    fun provideSmth(): Smth

}