package com.example.daggertest.di.component

import com.example.daggertest.activity.MainActivity
import com.example.daggertest.di.module.SmthModule
import com.example.daggertest.di.provider.ActivityProvider
import com.example.daggertest.di.provider.ApplicationProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [ApplicationProvider::class],
    modules = [SmthModule::class])
interface ActivityComponent : ActivityProvider {

    fun inject(activity: MainActivity)

    class Initializer private constructor() {

        companion object {

            fun init(application: ApplicationProvider): ActivityComponent = DaggerActivityComponent
                .builder()
                .applicationProvider(application)
                .build()
        }
    }
}