package com.example.daggertest.di.component

import com.example.daggertest.di.provider.ActivityProvider
import com.example.daggertest.fragment.FragmentB
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(dependencies = [ActivityProvider::class])
interface FragmentBComponent {

    fun inject(fragmentA: FragmentB)

    class Initializer private constructor() {

        companion object {

            fun init(activity: ActivityProvider): FragmentBComponent = DaggerFragmentBComponent.builder()
                .activityProvider(activity)
                .build()
        }
    }
}