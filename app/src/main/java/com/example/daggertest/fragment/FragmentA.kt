package com.example.daggertest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.daggertest.R
import com.example.daggertest.Smth
import com.example.daggertest.activity.IActivity
import com.example.daggertest.di.component.FragmentAComponent
import javax.inject.Inject

class FragmentA : Fragment() {

    companion object {

        fun newInstance() = FragmentA()
    }

    @Inject
    lateinit var smth: Smth

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    private fun inject() {
        FragmentAComponent.Initializer
            .init((activity as IActivity).getComponent())
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_a, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("!!! fragmentA smth=${smth.hashCode()}")
    }
}