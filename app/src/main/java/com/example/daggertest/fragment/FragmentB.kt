package com.example.daggertest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.daggertest.R
import com.example.daggertest.Smth
import com.example.daggertest.activity.IActivity
import com.example.daggertest.di.component.FragmentBComponent
import javax.inject.Inject

class FragmentB : Fragment() {

    companion object {

        fun newInstance() = FragmentB()
    }

    @Inject
    lateinit var smth: Smth

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    private fun inject() {
        FragmentBComponent.Initializer
            .init((activity as IActivity).getComponent())
            .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_b, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        println("!!! fragmentB smth=${smth.hashCode()}")
    }
}